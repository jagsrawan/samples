from datetime import datetime, time, timedelta
from time import mktime
import uuid
import cql
import pycassa


conn = cql.connect('127.0.0.1')
conn.set_cql_version('3.0.0')
conn.set_initial_keyspace('sensor')

pool = pycassa.ConnectionPool('sensor')
se_cf = pycassa.ColumnFamily(pool, 'sensor_entries')
st_cf = pycassa.ColumnFamily(pool, 'sensor_text')

pool = pycassa.ConnectionPool('aps')
ags_cf = pycassa.ColumnFamily(pool, 'ags_rollup_1day')


# put in 5 sensor reads right now

default_cols = ['cost', 'budg', 'bid', 'imps', 'cpm', 'clks', 'ctr', 'cpc', 'covs', 'cov1', 'cov2', 'cr','cpa', 'rcpa', 'fee', 'marg', 'spnt', 'acts', 'acqs', 'crev', 'down', 'qual', 'roi']

for i in range(10000): #30 accounts * 30 days

    key = uuid.uuid4()

    now = datetime.now()

    hour_count = 54
    for single_hour in (now + timedelta(hours=n) for n in range(0,hour_count,1)):
        for col in default_cols:
            ts = int( mktime(single_hour.timetuple()) )
            se_cf.insert(key, {(ts,col) :  "random sample {}".format(i) } )
            #st_cf.insert(key, {ts: "random sample {}".format(i) } )
'''
    query = """INSERT INTO sensor_entries 
                (sensorid, time_taken, reading)
                VALUES (:sensorid, :time_taken, :reading)"""
    values = {'sensorid': key,
              'time_taken': ts,
              'reading': "random sample {}".format(i)}
    cur = conn.cursor()
    cur.execute(query, values)
'''

