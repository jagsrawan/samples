from datetime import datetime, time, timedelta
from math import exp
from random import randint, seed
from time import mktime
import uuid
import cql
import pycassa
from time import clock


conn = cql.connect('127.0.0.1')
conn.set_cql_version('3.0.0')
conn.set_initial_keyspace('aps')

pool = pycassa.ConnectionPool('aps')
ags_cf = pycassa.ColumnFamily(pool, 'ags_rollup_1day_fast')

default_cols = ['cost', 'budg', 'bid', 'imps', 'cpm', 'clks', 'ctr', 'cpc', 'covs', 'cov1', 'cov2', 'cr','cpa', 'rcpa', 'fee', 'marg', 'spnt', 'acts', 'acqs', 'crev', 'down', 'qual', 'roi']


max_accounts = 1
max_days = 1 # Number of days to to run the simulation
max_aps_collections = 2 #24 hours + 30 days, total number of discrete takes.  Note we don't do the day thing exactly here.
max_adgroups = 1000

day_start = datetime.strptime('2013-02-01 00:00:00','%Y-%m-%d %H:%M:%S',)

acc_FBID = [ '%014d' % (pow(10,14) + n*pow(10,10))  for n in range(0,max_accounts,1)]


adgroup_FBID = dict()
for i,fbid in enumerate(acc_FBID):
    adgroup_FBID[fbid] =['%014d' % (pow(10,14) + i*pow(10,6)+n)  for n in range(0,max_adgroups)]


start = datetime.now()
for single_day in (day_start + timedelta(days=n) for n in range(0,max_days,1)):
    for fbid in acc_FBID:
        row_key = "%s:%s" % (fbid, single_day.strftime("%Y%m%d"))

        for single_hour in (single_day + timedelta(hours=n) for n in range(0,max_aps_collections,1)):
            ts = int( mktime(single_hour.timetuple()) )

            print ("Inserting new Column row_key %s, datetime %s" % (row_key,single_hour) )
            query_list = []

            data_map = dict( [(cols,i) for i,cols in enumerate(default_cols)] )
            for param, data in data_map.iteritems():
                for agfbid in adgroup_FBID[fbid]:

                    rk_map = dict({'accfbid_day' : "'%s:%s'" % ( str(row_key), param),
                                   'ts' : int(ts),
                                   'agfbid' : long(agfbid),
                                   'value' : data})

                    '''
                    ags_cf.insert( rk_map ['accfbid_day'], {( rk_map ['ts'], rk_map ['agfbid']) :  data} )
                    '''
                    k = ','.join( rk_map.keys())
                    v = ','.join( str(v) for v in rk_map.values())
                    query = """INSERT INTO ags_rollup_1day_fast
                                    (%s)
                                    VALUES (%s)""" % (k,v)
                    query_list.append(query)
                
                    if len(query_list) > 100:
                        cur= conn.cursor()
                        q = "BEGIN BATCH \n%s\n APPLY BATCH;" % '\n'.join(query_list) 
                        cur.execute(q)
                        query_list = []
                    
                   
            if len(query_list) > 0:
                cur= conn.cursor()
                q = "BEGIN BATCH \n%s\n APPLY BATCH;" % '\n'.join(query_list) 
                cur.execute(q)
            
end = datetime.now()
duration = end-start
print ( "Total Duration (%s)" % (duration) )


