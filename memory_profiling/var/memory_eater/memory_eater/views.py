from django.http import HttpResponse

from eater import consume_memory


def eat(request):
    from guppy import hpy
    heapy = hpy()
    heap = consume_memory(.5, heapy)
    return HttpResponse('<pre>{}</pre>'.format(heap))


def eatforever(request):
    consume_memory()
    return HttpResponse("You're dead")
