"""
wsgi_dozer.py

WSGI config for memory_eater project modified to use Dozer
"""

import os
import sys
sys.path.append('/var/django_projects/memory_eater')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "memory_eater.settings")

import django.core.handlers.wsgi
from dozer import Dozer

application = django.core.handlers.wsgi.WSGIHandler()
application = Dozer(application)
