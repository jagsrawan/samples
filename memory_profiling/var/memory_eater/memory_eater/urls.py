from django.conf.urls import patterns


urlpatterns = patterns('memory_eater.views',
                       (r'^eat$', 'eat'),
                       (r'^eatforever$', 'eatforever'))
