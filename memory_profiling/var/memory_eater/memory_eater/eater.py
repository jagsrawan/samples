"""
eater.py
"""
from random import betavariate
from string import uppercase

from psutil import virtual_memory


class Base(object):

    @classmethod
    def insert(class_, value):
        if not hasattr(class_, 'values'):
            list_name = 'List{}'.format(class_.__name__)
            exec('class {}(list): pass'.format(list_name))
            class_.values = eval('{}'.format(list_name))()
        class_.values.append(value)


def consume_memory(memory_delta=100, heapy=None):

    cutoff = virtual_memory().percent + memory_delta

    for char in uppercase:
        exec('class {}(Base): pass'.format(char))

    while virtual_memory().percent < cutoff:
        char = uppercase[int(betavariate(.9, 1) * 26)]
        eval(char).insert(eval(char)())

    return repr(heapy.heap())
