"""
WSGI config for memory_eater project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/howto/deployment/wsgi/
"""

import os
import sys
sys.path.append('/var/django_projects/memory_eater')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "memory_eater.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
